function putModule(path,next)
{
	var head= document.getElementsByTagName('head')[0];
	var script= document.createElement('script');
	script.type= 'text/javascript';
	if(next!==undefined && typeof(next=='function'))
	{
		script.onload = function(){
			next();
		}
		script.onreadystatechange = function(){
			if (script.readyState == "loaded" ||
					script.readyState == "complete"){
					script.onreadystatechange = null;
					next();
			}
		};
	}
	script.src= path;
	head.appendChild(script);
}

function putCss(path)
{
	var head= document.getElementsByTagName('head')[0];
	var fileref=document.createElement("link")
	fileref.setAttribute("rel", "stylesheet")
	fileref.setAttribute("type", "text/css")
	fileref.setAttribute("href", path)
	head.appendChild(fileref);
}

function loadView(path,parent,callback)
{
	$.ajax(path,
	{
		type:"GET",
		dataType:"json",
		context:{"parent":parent,"callback":callback},
		success:prepareView,
		error:errorLoadView
	});
}

function errorLoadView(request)
{
	console.log(request);
}
function prepareView(request)
{
	object = this;
	parent = $(object.parent);
	element_top = newElement(request.type,request.class,request.id,parent);
	prepareContentBlock(request.content,element_top);
	if(object.callback != null)
		object.callback();
}

function prepareContentBlock(content,parent)
{
	if(content instanceof  Array)
	{
		for(index in content)
		{
			block = content[index];
			element = newElementText(block.type,block.class,block.id,block.text,parent);
			prepareContentBlock(block.content,element);
		}
	}
}
function newElementText(type,classes,_id,text,parent)
{
	element = newElement(type,classes,_id,parent);
	element.text(text);
	return element;
}
function newElement(type,classes,_id,parent)
{
	element = $("<"+type+"/>",{
		id:_id,
		class:classes
	})
	element.appendTo(parent);
	return element;
}


function cookieLoad()
{
	putModule("js/vendors/jquery.cookie-1.4.1.min.js",start);
}

function start()
{
	var keyLogin = localStorage.keyLoign;
	if(keyLogin === undefined)
	{
		putModule("js/modules/auth.js");
	}else{
		putModule("js/modules/cabinet.js");
	}
}

function dummy()
{
}