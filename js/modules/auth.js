﻿function initModule()
{
	putCss("css/core.css");
	putCss("css/login.css");
	putModule("js/vendors/sha256.js");
	form = newElement("div","block rounded-5px default-backcolor","login-form",$("body"));
	generateForm(form);
}
initModule();

function generateForm(form)
{
	user_block = newElement("div","block","login-form-username-block",form);
	generationUserBlock(user_block);
	pass_block = newElement("div","block","login-form-password-block",form);
	generationPasswordBlock(pass_block);
	button = newElement("input","block","login-form-submit",form);
	button.attr("type","submit");
	button.click(function(){sendAuth();});
}

function generationUserBlock(block)
{
	label = newElement("span","block","login-form-username-block-label",block);
	label.text("Ваше имя");
	input = newElement("input","","login-form-username-block-input",block);
}

function generationPasswordBlock(block)
{
	label = newElement("span","block","login-form-password-block-label",block);
	label.text("Ваше пароль");
	input = newElement("input","","login-form-password-block-input",block);
}

function sendAuth()
{
	sh = new jsSHA($("#login-form-password-block-input").val(),"TEXT");
	hash = sh.getHash('SHA-256',"HEX");
	data = {mail:$("#login-form-username-block-input").val(),password:hash};
	$.ajax("http://95.170.88.200/web/login",{
		type:"POST",
		dataType:"json",
		data: data,//JSON.stringify(data),
		success:proccessAuth,
		error:proccessError
	})
}

function proccessAuth(request)
{
	localStorage.setItem("keyLoign",request.key);
	window.location.href=window.location.href
}

function proccessError(request)
{
	object = request.responseJSON;
	alert(object.message);
}