﻿var actionGLOBAL = "main"

function initModule()
{
	putCss("css/core.css");
	putCss("css/cabinet.css");
	putModule("js/vendors/sha256.js");
	putModule("https://maps.googleapis.com/maps/api/js?v=3.exp&callback=dummy");
	loadView('js/modules/views/cabinet/cabinet.json',"body",initAction);
	$("body").delegate(".item-menu","click",clickMenuItem);
	initAction();
}

initModule();

function clickMenuItem(event)
{
	item = $(event.currentTarget);
	$(".item-menu").each(function(){
		if($(this).is(".active"))
			$(this).toggleClass("active");
	});
	item.toggleClass("active");
	initAction();
}

function initAction()
{
	action = $(".active").attr("id");
	loadAction(action);
}

function loadAction(action)
{
	parent = $("div.content");
	if(parent!=undefined && parent.length>0)
	{
		pacleholder = newElement("div","loader-placeholder rounded-5px block",null,parent);
		actionGLOBAL = action;
		loadView('js/modules/views/loader.json',"div.loader-placeholder",beginLoadAction);
	}
}

function beginLoadAction()
{
	parent = $("div.content");
	parent.empty();
	switch(actionGLOBAL)
	{
		case "main":
			getActionMainData();
		break;
		case "onoff":
			getActionOnoffDate();
		break;
	}
}

function getActionMainData()
{		
	$.ajax("http://95.170.88.200/mobile?key="+localStorage.keyLoign,{
		type:"GET",
		dataType:"json",
		success:prepareMap
	});
	$.ajax("http://95.170.88.200/web/statistic?key="+localStorage.keyLoign,{
		type:"GET",
		dataType:"json",
		success:prepareStatistics
	});
}
function prepareStatistics(request)
{
	parent = $("div.content");
	vmp = newElement("div","block rounded-5px hide","statistic",parent);
	title = newElement("span","block","title",vmp);
	title.text("Заявок за сегодня");
	for(index in request.data)
	{
		param = request.data[index];
		block = newElement("div","inline statistic-block",null,vmp);		
		value = newElement("span","block statistic-value",null,block);
		value.text(param.c)
		info = newElement("span","block statistic-info",null,block);
		info.text(param.name)
	}
	vmp.fadeIn();
}
function prepareMap(request)
{
	parent = $("div.content");
	vmp = newElement("div","map hide","map",parent);
	var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
	var mapOptions = {
		zoom: 18,
		center: myLatlng
	};
	var map = new google.maps.Map(document.getElementById('map'), mapOptions);
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow(); 
	for(phoneIndex in request.phones)
	{
		phone = request.phones[phoneIndex];
		 var marker = new google.maps.Marker({
		position: new google.maps.LatLng(phone.ltd, phone.lng),
		map: map});
		bounds.extend(marker.position);
		google.maps.event.addListener(marker, 'click', (function(marker, name) {
			return function() {
			infowindow.setContent(name);
			infowindow.open(map, marker);
			}})(marker, phone.name));
	}	
	$("div.loader-placeholder").fadeOut();
	vmp.fadeIn();		 
    google.maps.event.trigger(map, 'resize');     
    map.fitBounds(bounds);	
}

function getActionOnoffDate()
{
	$.ajax("http://95.170.88.200/onoff?key="+localStorage.keyLoign+"&offset=0&count=50&fields=name,address,type,coords,comment,status&filter=status_id=1",
	{
		type:"GET",
		dataType:"json",
		success:prepareOnOff
	});
}

function prepareOnOff(request)
{
	parent = $("div.content");	
	parent.delegate(".onoff-element","click",clickOnoffItem);
	for(index in request.onoff)
	{
		onoff = request.onoff[index];
		vmp = newElement("div","inline rounded-5px hide onoff-element",onoff.id,parent);
		newElementText("span","block",null,onoff.address[1],vmp);
		newElementText("span","block",null,onoff.name,vmp);		
		if(onoff.type==1)
			newElementText("span","block",null,"Отключить",vmp);
		else
			newElementText("span","block",null,"Подключить",vmp);
		vmp.fadeIn();
	}
	$("div.loader-placeholder").fadeOut();
}

function clickOnoffItem (arr) {
	parent = $("div.content");
	vmp = newElement("div","rounded-5px hide","popup",parent);
	vmp.fadeIn();
}